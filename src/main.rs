//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::subcommand::SubCommandInit;
use clap::{App, AppSettings};
use log::LevelFilter;
use std::collections::HashMap;

mod catalogue;
mod config;
mod subcommand;

fn main() {
    // Initialize the logger
    env_logger::Builder::from_default_env()
        .filter(None, LevelFilter::Trace)
        .init();

    // Create the app and fill out some info to be displayed
    let mut app = App::new("a3run")
        .version("1.0")
        .about("Tool for managing and launching an ArmA 3 server through SteamCMD")
        .author("MindSpunk (Nathan Voglsam)")
        .setting(AppSettings::ArgRequiredElseHelp);

    // Create the hash map that will hold all our subcommand instances
    let mut subcommands = HashMap::new();

    // Init subcommands
    subcommand::Run::init(&mut subcommands, &mut app);
    subcommand::UpdateMods::init(&mut subcommands, &mut app);
    subcommand::UpdateServer::init(&mut subcommands, &mut app);
    subcommand::WriteDefaultConfig::init(&mut subcommands, &mut app);
    subcommand::RunHeadlessClient::init(&mut subcommands, &mut app);

    // Parse the command line arguments
    let matches = app.get_matches();

    // If we've got a subcommand we'll want to run it
    if let Some(command_name) = matches.subcommand_name() {
        // Get the parsed form of the sub command's arguments
        let matches = matches.subcommand_matches(command_name).unwrap();

        // Try and call the sub command, protecting against panics if a non existent command was
        // asked for
        if let Some(command) = subcommands.get_mut(command_name) {
            command.run(matches).unwrap();
        }
    }
}
