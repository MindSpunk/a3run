//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::config::Config;
use crate::subcommand::{SubCommand, SubCommandInit};
use clap::{App, Arg, ArgMatches};
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;

pub struct WriteDefaultConfig {}

impl SubCommand for WriteDefaultConfig {
    fn name(&self) -> &'static str {
        "write_default_config"
    }

    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()> {
        let path = matches.value_of("output").unwrap();
        let mut file = File::create(path)?;

        let json_out = Config::default();
        let json_out = serde_json::to_string_pretty(&json_out)?;

        file.write_all(json_out.as_bytes())?;

        log::info!("Successfully output default config to {}", path);

        Ok(())
    }
}

impl SubCommandInit for WriteDefaultConfig {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App) {
        let run = WriteDefaultConfig {};
        let name = run.name();

        map.insert(name, Box::new(run));

        let out_arg = Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("FILENAME")
            .help("The file to output the default config to")
            .default_value("a3run.json")
            .takes_value(true);

        let subcommand = clap::SubCommand::with_name(name)
            .about("Outputs a default config to the given file")
            .arg(out_arg);

        *app = app.clone().subcommand(subcommand);
    }
}
