//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::subcommand::{
    config_arg, load_config, standard_steamcmd_args, steam_cmd_command, SubCommand, SubCommandInit,
};
use clap::{App, ArgMatches};
use std::collections::HashMap;

pub struct UpdateServer {}

impl SubCommand for UpdateServer {
    fn name(&self) -> &'static str {
        "update_server"
    }

    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()> {
        let config = load_config(matches)?;

        log::info!("Updating/Installing server");

        let mut args = Vec::new();

        standard_steamcmd_args(
            &mut args,
            config.username(),
            config.password(),
            config.install_dir(),
        );

        args.push("+app_update".into());
        args.push("233780".into());

        if let Some(branch) = config.branch() {
            log::info!("Specified branch: {}", branch);
            args.push("-beta".into());
            args.push(branch.into());
        }

        args.push("validate".into());
        args.push("+quit".into());

        log::info!("Starting SteamCMD with these arguments");
        for arg in args.iter() {
            log::info!("{}", arg);
        }

        let mut command = steam_cmd_command(&config);
        command.args(args);

        let exit_status = command.status()?;
        if exit_status.success() {
            log::info!("Successfully updated arma3server\n");
            Ok(())
        } else {
            let err = if let Some(code) = exit_status.code() {
                format!("Call to steamcmd failed with exit code: {}", code)
            } else {
                "Call to steamcmd failed without an exit code".to_string()
            };
            log::error!("{}", &err);
            Err(std::io::Error::new(std::io::ErrorKind::Other, err))
        }
    }
}

impl SubCommandInit for UpdateServer {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App) {
        let run = UpdateServer {};
        let name = run.name();

        map.insert(name, Box::new(run));

        let config_arg = config_arg();

        let subcommand = clap::SubCommand::with_name(name)
            .about("Installs and/or updates the ARMA3 server from steam")
            .arg(config_arg);

        *app = app.clone().subcommand(subcommand);
    }
}
