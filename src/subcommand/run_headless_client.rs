//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::catalogue::ModType;
use crate::subcommand::{
    config_arg, load_config, load_mods, mods_args, SubCommand, SubCommandInit,
};
use clap::{App, ArgMatches};
use std::collections::HashMap;
use std::path::Path;
use std::process::{Command, Stdio};
use std::str::FromStr;

pub struct RunHeadlessClient {}

impl SubCommand for RunHeadlessClient {
    fn name(&self) -> &'static str {
        "run_headless_client"
    }

    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()> {
        // Load the config and the two mod lists
        let config = load_config(matches)?;
        let mods = load_mods(matches)?;
        let ip = matches.value_of("address").unwrap();

        // Parse the number of headless clients to create from the arguments
        let count = matches.value_of("number").unwrap();
        let count = i32::from_str(count)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        let count = count.max(1); // Clamp to at least be 1

        log::info!("Running ArmA3 Headless Client");
        log::info!("Instance count: {}", count);

        // Load the install_dir and executable path
        let run_dir = Path::new(config.arma_dir()).to_path_buf();
        let exepath = if cfg!(target_os = "windows") {
            if config.is_x64() {
                run_dir.join("arma3server_x64.exe")
            } else {
                run_dir.join("arma3server.exe")
            }
        } else {
            if config.is_x64() {
                run_dir.join("arma3server_x64")
            } else {
                run_dir.join("arma3server")
            }
        };
        log::info!("Install Directory: {:#?}", &run_dir);
        log::info!("Executable Path: {:#?}", &exepath);

        // Canonicalize as the program path handling is inconsistent across platforms
        let exepath = exepath.canonicalize()?;

        log::info!("Executable Absolute Path: {:#?}", &exepath);

        // Log the mods that will be loaded
        for m in mods.mods.iter() {
            match &m.path {
                ModType::Workshop(id) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Mod: {}", name);
                    } else {
                        log::info!("Loading Mod: {}", id);
                    };
                }
                ModType::LocalMod(path) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Mod: {}", name);
                    } else {
                        log::info!("Loading Mod: {}", path);
                    };
                }
            };
        }

        // Create `count` number of `Command` instances
        let mut commands: Vec<Command> = (0..count)
            .map(|_| {
                let mut command = Command::new(&exepath);
                command.current_dir(&run_dir);

                command.arg("-client");

                command.arg(format!("-connect={}", ip));

                if let Some(password) = matches.value_of("password") {
                    command.arg(format!("-password={}", password));
                }

                for m in mods.mods.iter() {
                    let arg = match &m.path {
                        ModType::Workshop(id) => {
                            if let Some(name) = &m.name {
                                log::info!("Loading Mod: {}", name);
                            } else {
                                log::info!("Loading Mod: {}", id);
                            };
                            format!(
                                "-mod=steamapps{sep}workshop{sep}content{sep}107410{sep}{id}",
                                sep = std::path::MAIN_SEPARATOR,
                                id = id
                            )
                        }
                        ModType::LocalMod(path) => {
                            if let Some(name) = &m.name {
                                log::info!("Loading Mod: {}", name);
                            } else {
                                log::info!("Loading Mod: {}", path);
                            };
                            format!("-mod={}", path)
                        }
                    };
                    command.arg(arg);
                }

                // Nullify stdout/err as it's probably not useful and could be mixed with several
                // instances stdout as well
                command.stdout(Stdio::null());
                command.stderr(Stdio::null());
                command.stdin(Stdio::inherit());

                command
            })
            .collect();

        let mut children = Vec::new();
        for command in commands.iter_mut() {
            children.push(command.spawn()?);
        }

        for child in children.iter_mut() {
            let exit_status = child.wait()?;
            if exit_status.success() {
                log::info!("Successfully ran arma3server as headless client\n");
            } else {
                let err = if let Some(code) = exit_status.code() {
                    format!("Call to arma3server failed with exit code: {}", code)
                } else {
                    "Call to arma3server failed without an exit code".to_string()
                };
                log::error!("{}", &err);
            }
        }

        Ok(())
    }
}

impl SubCommandInit for RunHeadlessClient {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App) {
        let run = RunHeadlessClient {};
        let name = run.name();

        map.insert(name, Box::new(run));

        let config_arg = config_arg();
        let mods_arg = mods_args();
        let ip_arg = clap::Arg::with_name("address")
            .long("address")
            .short("a")
            .value_name("SERVER_IP")
            .help("The IP of the server to connect to as a headless client")
            .default_value("127.0.0.1")
            .takes_value(true);

        let pass_arg = clap::Arg::with_name("password")
            .long("password")
            .short("p")
            .value_name("SERVER_PASSWORD")
            .help("The password of the server being connected to")
            .takes_value(true);

        let count_arg = clap::Arg::with_name("number")
            .long("number")
            .short("n")
            .value_name("CLIENT_NUMBER")
            .help("The number of headless client instances to launch")
            .default_value("1")
            .takes_value(true);

        let subcommand = clap::SubCommand::with_name(name)
            .about("Runs the server as a headless client")
            .arg(config_arg)
            .arg(mods_arg)
            .arg(ip_arg)
            .arg(pass_arg)
            .arg(count_arg);

        *app = app.clone().subcommand(subcommand);
    }
}
