//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

mod run;
mod run_headless_client;
mod update_mods;
mod update_server;
mod write_default_config;

pub use run::Run;
pub use run_headless_client::RunHeadlessClient;
pub use update_mods::UpdateMods;
pub use update_server::UpdateServer;
pub use write_default_config::WriteDefaultConfig;

use crate::catalogue::Catalogue;
use crate::config::Config;
use clap::{App, Arg, ArgMatches};
use std::any::Any;
use std::collections::HashMap;
use std::io::Read;
use std::process::Command;

///
/// A generic sub command interface
///
pub trait SubCommand: Any {
    fn name(&self) -> &'static str;
    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()>;
}

///
/// Interface for initializing a SubCommand object into the hashmap used for mapping the subcommand
/// chosen at runtime to the underlying implementation
///
pub trait SubCommandInit {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App);
}

///
/// Function for defining a common argument that will be used across multiple sub commands.
///
/// Most sub commands will allow for specifying a config to use so DRY
///
fn config_arg() -> Arg<'static, 'static> {
    Arg::with_name("config")
        .short("c")
        .long("config")
        .value_name("FILENAME")
        .default_value("a3run.json")
        .help("Selects a specific config to use")
        .takes_value(true)
}

///
/// Function for getting the config path from the argument matches and then loading it from a file
///
fn load_config(matches: &ArgMatches) -> std::io::Result<Config> {
    let path = matches.value_of("config").unwrap();
    Config::new_from_path(path)
}

///
/// Returns two clap arguments for specifying individual mods and mod packs to load
///
/// # Returns
///
/// (mods list, packs list)
///
fn mods_args() -> Arg<'static, 'static> {
    let mods_arg = Arg::with_name("mods")
        .short("m")
        .long("mods")
        .value_name("MODS")
        .help("Path to a list of mods to load")
        .takes_value(true);
    mods_arg
}

///
/// Resolve the list of mods and packs to load down to a flat, de duplicated list of mods to load
///
fn load_mods(matches: &ArgMatches) -> std::io::Result<Catalogue> {
    if let Some(path) = matches.value_of("mods") {
        // Open the file
        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(path)?;

        // Read the file
        let mut text = String::new();
        file.read_to_string(&mut text)?;

        serde_json::from_str(&text)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err.to_string()))
    } else {
        Ok(Catalogue { mods: Vec::new() })
    }
}

///
/// Returns two clap arguments for specifying individual server only mods and mod packs to load.
///
/// # Returns
///
/// (server mods list, server packs list)
///
fn server_mods_args() -> Arg<'static, 'static> {
    let server_mods_arg = Arg::with_name("smods")
        .short("s")
        .long("smods")
        .value_name("MODS")
        .help("Path to a list of server only mods to use")
        .takes_value(true);
    server_mods_arg
}

///
/// Resolve the list of server mods and packs to load down to a flat, de duplicated list of mods to
/// load
///
fn load_server_mods(matches: &ArgMatches) -> std::io::Result<Catalogue> {
    if let Some(path) = matches.value_of("smods") {
        // Open the file
        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(path)?;

        // Read the file
        let mut text = String::new();
        file.read_to_string(&mut text)?;

        serde_json::from_str(&text)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err.to_string()))
    } else {
        Ok(Catalogue { mods: Vec::new() })
    }
}

///
/// Returns two clap arguments for specifying individual mods and mod packs to load
///
/// # Returns
///
/// (mods list, packs list)
///
fn allowed_mods_args() -> Arg<'static, 'static> {
    let mods_arg = Arg::with_name("amods")
        .short("a")
        .long("amods")
        .value_name("MODS")
        .help("Path to a list of allowed mods to allow clients to use without them loaded on the server")
        .takes_value(true);
    mods_arg
}

///
/// Resolve the list of cmods and packs to load down to a flat, de duplicated list of mods to allow
/// on a client
///
fn load_allowed_mods(matches: &ArgMatches) -> std::io::Result<Catalogue> {
    if let Some(path) = matches.value_of("amods") {
        // Open the file
        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(path)?;

        // Read the file
        let mut text = String::new();
        file.read_to_string(&mut text)?;

        serde_json::from_str(&text)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err.to_string()))
    } else {
        Ok(Catalogue { mods: Vec::new() })
    }
}

///
/// Function for adding the standard required arguments for running the arma3server executable
///
#[inline]
fn standard_run_args(
    c: &mut Command,
    port: i16,
    a3config: &str,
    basic_config: Option<&str>,
    profile: &str,
) {
    c.arg("-profiles=PROFILES");

    let profile_arg = format!("-name={}", profile);
    c.arg(profile_arg);

    let port_arg = format!("-port={}", port);
    c.arg(port_arg);

    let cfg_arg = format!("-config={}", a3config);
    c.arg(cfg_arg);

    if let Some(basic_config) = basic_config {
        let cfg_arg = format!("-cfg={}", basic_config);
        c.arg(cfg_arg);
    }

    c.arg("-world=empty");
}

///
/// Function for adding the standard required arguments for running the steamcmd executable
///
#[inline]
fn standard_steamcmd_args(
    args: &mut Vec<String>,
    user: &str,
    pass: Option<&str>,
    install_dir: &str,
) {
    args.push("+login".into());

    args.push(user.into());

    if let Some(pass) = pass {
        args.push(pass.into());
    }

    args.push("+force_install_dir".into());

    args.push(install_dir.into());
}

///
/// Builds a Command object for executing steamcmd. The specifics are platform dependent so we use
/// this function to wrap that platform implementation
///
/// # Info
///
/// We need this function because on Windows steamcmd is a binary executable file while on Linux
/// steamcmd uses a bash script so we have to call `bash` with steamcmd.sh as an argument rather
/// than call steamcmd.sh directly
///
fn steam_cmd_command(config: &Config) -> Command {
    if cfg!(target_os = "windows") {
        Command::new(config.steam_cmd_name().unwrap_or("steamcmd.exe"))
    } else {
        if config.use_script() {
            let mut cwd = std::env::current_dir().expect("Failed to get cwd");
            cwd.push("linux32");

            let new_lib_path: String = if let Ok(lib_path) = std::env::var("LD_LIBRARY_PATH") {
                [cwd.to_str().unwrap(), &lib_path].join(":")
            } else {
                cwd.to_str().unwrap().to_string()
            };

            cwd.push(config.steam_cmd_name().unwrap_or("steamcmd"));

            let mut c = Command::new(cwd);
            c.env("LD_LIBRARY_PATH", new_lib_path);

            //c.arg("steamcmd.sh");
            c
        } else {
            Command::new(config.steam_cmd_name().unwrap_or("steamcmd"))
        }
    }
}
