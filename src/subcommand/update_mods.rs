//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::catalogue::ModType;
use crate::subcommand::{
    config_arg, load_config, load_mods, load_server_mods, mods_args, server_mods_args,
    standard_steamcmd_args, steam_cmd_command, SubCommand, SubCommandInit,
};
use clap::{App, ArgMatches};
use std::collections::HashMap;

pub struct UpdateMods {}

impl SubCommand for UpdateMods {
    fn name(&self) -> &'static str {
        "update_mods"
    }

    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()> {
        let config = load_config(matches)?;
        let mods = load_mods(matches)?;
        let smods = load_server_mods(matches)?;

        log::info!("Updating mods:\n: {:?}\n:", mods);

        // Just early exit as no mods were asked to be updated
        if mods.mods.is_empty() && smods.mods.is_empty() {
            return Ok(());
        }

        let mut args = Vec::new();

        standard_steamcmd_args(
            &mut args,
            config.username(),
            config.password(),
            config.install_dir(),
        );

        log::info!("Starting SteamCMD with these arguments");
        for arg in args.iter() {
            log::info!("{}", arg);
        }

        let mut command = steam_cmd_command(&config);
        command.args(args);

        for m in mods.mods.iter().chain(smods.mods.iter()) {
            match &m.path {
                ModType::Workshop(id) => {
                    if let Some(name) = &m.name {
                        log::info!("Updating Mod: {}", name);
                    } else {
                        log::info!("Updating Mod: {}", id);
                    };
                    command.arg("+workshop_download_item");
                    command.arg("107410");
                    command.arg(id.as_str());
                    command.arg("validate");
                }
                ModType::LocalMod(path) => {
                    if let Some(name) = &m.name {
                        log::info!("Skipping Local Mod: {}", name);
                    } else {
                        log::info!("Skipping Local Mod: {}", path);
                    };
                }
            };
        }

        let exit_status = command.status()?;
        if exit_status.success() {
            println!("Successfully updated mods\n");
            Ok(())
        } else {
            let err = if let Some(code) = exit_status.code() {
                format!("Call to steamcmd failed with exit code: {}", code)
            } else {
                "Call to steamcmd failed without an exit code".to_string()
            };
            log::error!("{}", &err);
            Err(std::io::Error::new(std::io::ErrorKind::Other, err))
        }
    }
}

impl SubCommandInit for UpdateMods {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App) {
        let run = UpdateMods {};
        let name = run.name();

        map.insert(name, Box::new(run));

        let config_arg = config_arg();
        let mods_arg = mods_args();
        let smods_arg = server_mods_args();

        let subcommand = clap::SubCommand::with_name(name)
            .about("Installs and/or updates mods from the workshop")
            .arg(config_arg)
            .arg(mods_arg)
            .arg(smods_arg);

        *app = app.clone().subcommand(subcommand);
    }
}
