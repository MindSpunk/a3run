//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use crate::catalogue::ModType;
use crate::subcommand::{
    allowed_mods_args, config_arg, load_allowed_mods, load_config, load_mods, load_server_mods,
    mods_args, server_mods_args, standard_run_args, SubCommand, SubCommandInit,
};
use clap::{App, ArgMatches};
use std::collections::HashMap;
use std::path::Path;
use std::process::Command;

///
/// A list of all the .bikey files that ship with the vanilla game
///
const ARMA3_KEYS: [&'static str; 5] = [
    "a3.bikey",
    "a3c.bikey",
    "gm.bikey",
    "csla.bikey",
    "vn.bikey",
];

///
/// A list of candidate key folder names for the server's keys folder. ArmA doesn't have strong
/// history of playing nice with case sensitive file systems so we check both to be on the safe side
///
const ARMA3_KEY_FOLDERS: [&'static str; 2] = ["Keys", "keys"];

///
/// A list of candidate key folder names for mod keys.
///
const MOD_KEY_FOLDERS: [&'static str; 10] = [
    "Key",
    "key",
    "Keys",
    "keys",
    "serverkeys",
    "serverkey",
    "Serverkeys",
    "Serverkey",
    "ServerKeys",
    "ServerKey",
];

pub struct Run {}

impl SubCommand for Run {
    fn name(&self) -> &'static str {
        "run"
    }

    fn run(&mut self, matches: &ArgMatches) -> std::io::Result<()> {
        // Load the config and the two mod lists
        let config = load_config(matches)?;
        let mods = load_mods(matches)?;
        let smods = load_server_mods(matches)?;
        let amods = load_allowed_mods(matches)?;

        log::info!("Running ArmA3 Server");

        // Load the install_dir and executable path
        let run_dir = Path::new(config.arma_dir()).to_path_buf();
        let exepath = if cfg!(target_os = "windows") {
            if config.is_x64() {
                run_dir.join("arma3server_x64.exe")
            } else {
                run_dir.join("arma3server.exe")
            }
        } else {
            if config.is_x64() {
                run_dir.join("arma3server_x64")
            } else {
                run_dir.join("arma3server")
            }
        };
        log::info!("Install Directory: {:#?}", &run_dir);
        log::info!("Executable Path: {:#?}", &exepath);

        log::info!("Clearing all .bikey files in the keys folder");
        log::info!("ArmA3 base keys will remain");
        ARMA3_KEYS
            .iter()
            .for_each(|key| log::info!("ArmA3 base key: {}", key));

        // Resolve the key folder to one of the two candidates
        let mut keys_folder_iter = ARMA3_KEY_FOLDERS.iter().filter_map(|folder_name| {
            let path = run_dir.join(folder_name);
            let exists = path.exists();
            let is_dir = path.is_dir();

            log::info!("Trying to find keys folder in {:#?}", &path);
            if exists && is_dir {
                Some(path)
            } else {
                log::info!("Failed to find keys folder in {:#?}", &path);
                None
            }
        });
        let keys_folder = if let Some(folder) = keys_folder_iter.next() {
            log::info!("Selected {:#?} as the server's keys folder", &folder);
            folder
        } else {
            let err = "No server keys folder was found";
            log::error!("{}", err);
            return Err(std::io::Error::new(std::io::ErrorKind::Other, err));
        };

        // Iterate through and delete all bikey files in the keys directory
        for entry in keys_folder.read_dir()? {
            let entry = entry?;

            // Assert is a file
            let file_type = entry.file_type()?;
            let is_file = file_type.is_file();

            // Assert is a bikey
            let is_bikey = if let Some(ext) = entry.path().extension() {
                ext == "bikey"
            } else {
                false
            };

            // Assert is not a base ArmA3 bikey
            let is_base_key = ARMA3_KEYS
                .iter()
                .filter(|key| **key == &entry.file_name())
                .count()
                > 0;

            // Perform actions based on the above derived conditions
            match (is_file, is_bikey, is_base_key) {
                (true, true, false) => {
                    let path = entry.path();
                    log::info!("Removed ({:#?}) key", &path);
                    std::fs::remove_file(&path)?;
                }
                (true, true, true) => {
                    log::trace!("Found ({:#?}) base key", entry.file_name());
                }
                (false, _, _) => {
                    log::warn!(
                        "Item ({:#?}) in keys folder is not a file",
                        entry.file_name()
                    );
                }
                (true, false, _) => {
                    log::warn!(
                        "Item ({:#?}) in keys folder is not a bikey",
                        entry.file_name()
                    );
                }
            }
        }

        log::info!("Copying needed mod keys into keys folder");
        // Copy the mod keys over
        let mod_keys_folder_iter = mods.mods.iter().chain(amods.mods.iter()).filter_map(|m| {
            log::trace!("Checking for keys for mod: NAME {:?}", m.name);

            // Get the path to the mod root folder
            let path = match &m.path {
                ModType::Workshop(path) => run_dir
                    .join("./steamapps/workshop/content/107410/")
                    .join(path),
                ModType::LocalMod(path) => Path::new(path).to_path_buf(),
            };

            // Read all child entries
            let read = path.read_dir().ok()?;
            for entry in read {
                let entry = entry.ok()?;
                let file_type = entry.file_type().ok()?;

                // Look for sub-directories
                if file_type.is_dir() {
                    // Filter to only ones with an expected set of names
                    let mut keys_folder_iter = MOD_KEY_FOLDERS.iter().filter_map(|name| {
                        let folder = entry.path();
                        if *name == &entry.file_name() {
                            log::info!("Copying from key folder {:#?}", &folder);
                            Some(folder)
                        } else {
                            None
                        }
                    });
                    if let Some(folder) = keys_folder_iter.next() {
                        return Some(folder);
                    }
                }
            }
            None
        });

        for mod_keys_folder in mod_keys_folder_iter {
            let read = mod_keys_folder.read_dir()?;
            for entry in read {
                let entry = entry?;
                let file_type = entry.file_type()?;
                let key_path = entry.path();

                // Assert is a file
                let is_file = file_type.is_file();

                // Assert is a bikey
                let is_bikey = if let Some(ext) = key_path.extension() {
                    ext == "bikey"
                } else {
                    false
                };

                if is_file && is_bikey {
                    let mod_key_name = key_path.file_name().unwrap();
                    let from = &key_path;
                    let to = keys_folder.join(mod_key_name);
                    log::info!("Copying key {:#?}", from);
                    std::fs::copy(from, to)?;
                }
            }
        }

        // Canonicalize as the program path handling is inconsistent across platforms
        let exepath = exepath.canonicalize()?;

        log::info!("Executable Absolute Path: {:#?}", &exepath);

        let mut command = Command::new(&exepath);
        command.current_dir(&run_dir);

        standard_run_args(
            &mut command,
            config.port(),
            config.a3config(),
            config.basic_config(),
            config.profile_name(),
        );

        for m in mods.mods.iter() {
            let arg = match &m.path {
                ModType::Workshop(id) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Mod: {}", name);
                    } else {
                        log::info!("Loading Mod: {}", id);
                    };
                    format!(
                        "-mod=steamapps{sep}workshop{sep}content{sep}107410{sep}{id}",
                        sep = std::path::MAIN_SEPARATOR,
                        id = id
                    )
                }
                ModType::LocalMod(path) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Mod: {}", name);
                    } else {
                        log::info!("Loading Mod: {}", path);
                    };
                    format!("-mod={}", path)
                }
            };
            command.arg(arg);
        }

        for m in smods.mods.iter() {
            let arg = match &m.path {
                ModType::Workshop(id) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Server Mod: {}", name);
                    } else {
                        log::info!("Loading Server Mod: {}", id);
                    };
                    format!(
                        "-serverMod=steamapps{sep}workshop{sep}content{sep}107410{sep}{id}",
                        sep = std::path::MAIN_SEPARATOR,
                        id = id
                    )
                }
                ModType::LocalMod(path) => {
                    if let Some(name) = &m.name {
                        log::info!("Loading Server Mod: {}", name);
                    } else {
                        log::info!("Loading Server Mod: {}", path);
                    };
                    format!("-serverMod={}", path)
                }
            };
            command.arg(arg);
        }

        for a in amods.mods.iter() {
            match &a.path {
                ModType::Workshop(id) => {
                    if let Some(name) = &a.name {
                        log::info!("Allowing Mod: {}", name);
                    } else {
                        log::info!("Allowing Mod: {}", id);
                    };
                }
                ModType::LocalMod(path) => {
                    if let Some(name) = &a.name {
                        log::info!("Allowing Mod: {}", name);
                    } else {
                        log::info!("Allowing Mod: {}", path);
                    };
                }
            };
        }

        let exit_status = command.status()?;
        if exit_status.success() {
            log::info!("Successfully ran arma3server\n");
            Ok(())
        } else {
            let err = if let Some(code) = exit_status.code() {
                format!("Call to arma3server failed with exit code: {}", code)
            } else {
                "Call to arma3server failed without an exit code".to_string()
            };
            log::error!("{}", &err);
            Err(std::io::Error::new(std::io::ErrorKind::Other, err))
        }
    }
}

impl SubCommandInit for Run {
    fn init(map: &mut HashMap<&'static str, Box<dyn SubCommand>>, app: &mut App) {
        let run = Run {};
        let name = run.name();

        map.insert(name, Box::new(run));

        let config_arg = config_arg();
        let mods_arg = mods_args();
        let server_mods_arg = server_mods_args();
        let client_mods_arg = allowed_mods_args();

        let subcommand = clap::SubCommand::with_name(name)
            .about("Executes an instance of the server with the specified list of mods")
            .arg(config_arg)
            .arg(mods_arg)
            .arg(server_mods_arg)
            .arg(client_mods_arg);

        *app = app.clone().subcommand(subcommand);
    }
}
