//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

use serde::Deserialize;
use serde::Serialize;

///
/// A mod catalogue
///
#[derive(Serialize, Deserialize, Debug)]
pub struct Catalogue {
    ///
    /// A list of mods
    ///
    pub mods: Vec<Mod>,
}

///
/// An individual mod listing in a catalogue
///
#[derive(Serialize, Deserialize, Debug)]
pub struct Mod {
    ///
    /// An optional name for the mod, purely for commenting purposes
    ///
    pub name: Option<String>,

    ///
    /// The
    ///
    pub path: ModType,
}

///
/// Sum type of the two types of mods
///
#[derive(Serialize, Deserialize, Debug)]
pub enum ModType {
    ///
    /// A workshop mod, this should contain the steam mod id of the mod
    ///
    Workshop(String),

    ///
    /// A non workshop mod that is found on the local filesystem. Should contain an absolute path
    /// or a path relative to the arma3server executable that points to the mod folder
    ///
    LocalMod(String),
}
