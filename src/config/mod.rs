//
// This file is a part of a3run
// https://gitlab.com/MindSpunk/a3run
//
//
// Copyright (c) 2020 Nathan Voglsam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

mod auth;

pub use auth::AuthInfo;

use serde::{Deserialize, Serialize};

use std::fs::File;
use std::io::Read;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    port: i16,
    profile_name: String,
    config: String,
    basic_config: Option<String>,
    install_dir: String,
    run_dir: Option<String>,
    branch: Option<String>,
    auth: AuthInfo,
    x64: bool,
    use_script: Option<bool>,
    steam_cmd_name: Option<String>,
}

impl Default for Config {
    #[inline]
    fn default() -> Self {
        Config {
            port: 2302,
            profile_name: "server".into(),
            config: "CONFIG_server.cfg".into(),
            basic_config: None,
            install_dir: "./".into(),
            run_dir: None,
            branch: None,
            auth: AuthInfo::new(),
            x64: false,
            use_script: Some(false),
            steam_cmd_name: None,
        }
    }
}

impl Config {
    pub fn new_from_path<P: AsRef<Path>>(path: P) -> std::io::Result<Config> {
        let mut file = File::open(path)?;

        let mut json_in = String::new();
        file.read_to_string(&mut json_in)?;

        serde_json::from_str(json_in.as_str())
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err.to_string()))
    }

    #[inline]
    pub fn profile_name(&self) -> &str {
        &self.profile_name
    }

    #[inline]
    pub fn username(&self) -> &str {
        &self.auth.username()
    }

    #[inline]
    pub fn password(&self) -> Option<&str> {
        self.auth.password()
    }

    #[inline]
    pub fn branch(&self) -> Option<&str> {
        self.branch.as_ref().map(String::as_str)
    }

    #[inline]
    pub fn port(&self) -> i16 {
        self.port
    }

    #[inline]
    pub fn a3config(&self) -> &str {
        &self.config
    }

    #[inline]
    pub fn basic_config(&self) -> Option<&str> {
        self.basic_config.as_ref().map(|v| v.as_str())
    }

    #[inline]
    pub fn install_dir(&self) -> &str {
        &self.install_dir
    }

    #[inline]
    pub fn run_dir(&self) -> Option<&str> {
        self.run_dir.as_ref().map(String::as_str)
    }

    #[inline]
    pub fn arma_dir(&self) -> &str {
        self.run_dir().unwrap_or(self.install_dir())
    }

    #[inline]
    pub fn is_x64(&self) -> bool {
        self.x64
    }

    #[inline]
    pub fn use_script(&self) -> bool {
        match self.use_script {
            None => true,
            Some(v) => v,
        }
    }

    #[inline]
    pub fn steam_cmd_name(&self) -> Option<&str> {
        match &self.steam_cmd_name {
            None => None,
            Some(v) => Some(v.as_str()),
        }
    }
}
