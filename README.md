# A3Run

A simple command line utility for managing an ArmA3 server, written in Rust.

## Information

Provides a singular executable interface for updating an ArmA3 server, as well as updating mods from the workshop and launching the server.

The most significant feature of A3Run is the ability to launch the server while selecting a list of named mods easily and it's ability to pull mods from the workshop transparently. By using a simple json config adding mods to the list is simple and pointing A3Run at the correct paths takes just seconds.